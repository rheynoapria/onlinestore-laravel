<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'home@index');
Route::get('/contact', 'home@contact');
Route::post('/pushData','home@store');
Route::get('/login','Login@index');
Route::post('/Register','Login@Register');
Route::post('/Signin','Login@Login');
Route::get('/logout','Login@Logout');
Route::get('/detail-item/{id_barang}','detail@index');
Route::post('/AddCart', 'order@Order');
Route::get('/cart/delete/{id_keranjang}','order@remove_item');

Route::get('/cart','order@keranjang');
Route::post('/Konfirmasi_pembayaran','order@confirm');
Route::post('/Konfirm','order@confirm_simpan');
Route::get('/confirm','order@confirmm');
Route::get('/checkout','order@Checkout');
Route::get('/checkout_list','order@checkout_list');

Route::get('/shop','shop@index');

// brand route
Route::get('/shop/acer','shop@acer');
Route::get('/shop/lenovo','shop@lenovo');
Route::get('/shop/dell','shop@dell');
Route::get('/shop/asus','shop@asus');
Route::get('/shop/hp','shop@hp');

// category route
Route::get('/shop/notebook','shop@notebook');
Route::get('/shop/ultrabook','shop@ultrabook');
Route::get('/shop/workstation','shop@workstation');
Route::get('/shop/gaming_laptop','shop@gaming');


// admin route
Route::get('/admin','AdminController@login');
Route::post('/validasi','AdminController@validasi');
Route::get('/admin/dashboard','AdminController@dashboard');
Route::get('/admin-logout','AdminController@logout');
