@extends('layout.master')
@section('main')
<?php
	$nama_user = session()->get('nama');
?>
    	<section id="cart_items" >
		<div class="container" style="margin-top:190px;">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info" style="top:10px;">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php $total = 0;
							$total_rupiah = 0;
						?>
					
						@foreach ($keranjang as $krj)
						@if (session()->has('id'))
							@if (session()->get('nama')===$krj->nama_user)
							<tr>
								<td class="cart_product">
									<img src="/data_file/{{$krj->gambar}}" alt="" style="width:220px;height:200px;">
								</td>
								<td class="cart_description">
									<h4><a href=""> {{$krj->nama_produk}}</a></h4>
									<p>{{$krj->jenis}}</p>
								</td>
								<td class="cart_price">
									<?php $harga_rupiah = floatval($krj->harga); 
									$rupiah =  number_format($harga_rupiah,0,'.','.'); ?>
									<p>Rp {{$rupiah}}</p>
								</td>
								<td class="cart_quantity">
									{{$krj->jumlah}}
								</td>
								<td class="cart_total">
									<?php $total_harga = $harga_rupiah * $krj->jumlah;
									$total_rupiah = number_format($total_harga,0,'.','.');
									?>
									<p class="cart_total_price">Rp {{$total_rupiah}}</p>
								</td>
								<td class="cart_delete">
								<a class="cart_quantity_delete" href="/cart/delete/{{$krj->id_keranjang}}"><i class="fa fa-times"></i></a>
								</td>
							</tr>
							<?php 
							$total += ($krj->jumlah * $harga_rupiah); 
							$total_rupiah =  number_format($total,0,'.','.');
							?>
							@endif

						@else	
						@endif
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
    </section> <!--/#cart_items-->
    

	<section id="do_action">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-6">
					
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<?php 
							
							?>
							<li>Total <span>Rp {{$total_rupiah}}</span></li>
						</ul>
							<a class="btn btn-default check_out" href="/checkout">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection