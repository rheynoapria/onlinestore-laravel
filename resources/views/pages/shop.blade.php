@extends('layout.nobanner')
@section('main')
<div class="col-sm-9 padding-right" style="margin-top:190px;">
	
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						@foreach ($barang as $brg)
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="/data_file/{{$brg->gambar}}" alt="" />
										<?php
											$angka = floatval($brg->harga);
											$rupiah =  number_format($angka,0,'.','.');
										?>
										<h2>Rp {{$rupiah}}</h2>
										<p><b> {{$brg->nama_produk}}</b></p>
										@if (session()->has('id'))
											
											<button data-toggle="modal" data-target="#myModal"  data-id="{{ $brg->id_barang }}" class="btn btn-default add-to-cart jumlah"><i class="fa fa-shopping-cart"></i>Add to cart</button>


										@else
											
										@endif
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
											<h2> Rp {{$rupiah}}</h2>
											<p> {{$brg -> nama_produk}} </p>
                                           	@if (session()->has('id'))
											<button data-toggle="modal" data-target="#myModal"  data-id="{{ $brg->id_barang }}" class="btn btn-default add-to-cart jumlah"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											@else
												
											@endif
											<a href="/detail-item/{{$brg->id_barang}}" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						
						
						{{-- <ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul> --}}
					</div><!--features_items-->
				</div>    
@endsection