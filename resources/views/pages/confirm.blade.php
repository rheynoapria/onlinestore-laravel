@extends('layout.master')
@section('main')
<section id="form"><!--form-->
    <div class="container" style="margin-top:190px;>
            <div class="row">
            @if (session()->has('id'))
                <div class="col-sm-4 col-sm-offset-1">
                        <div class="contact-info">
                                <h2 class="title text-center">Tata Cara Pembayaran</h2>
                                <address>
                                <p>Pembayaran pesanan anda dapat dilakukan dengan mentransfer nominal harga total ke nomor rekning dibawah ini</p><br><br>
                                <img src="{{ asset('images/frontend_images/bca_logo.png')}}" class="rounded" alt="confirm images" style="">
                                <p><b> BANK BCA NO. REK 814214121 A.N Lenore shop</b></p><br><br>
                                <p></p>
                                <p></p>
                                <p>Mohon konfirmasi pembayaran anda dengan cara mengupload bukti transaksi anda</p>
                                <p>Email: info@lenore.co.id</p>
                                </address>
                        </div>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                        <div class="login-form"><!--login form-->
                                <h2>Konfirmasi Pembayaran</h2>
                                <form action="/Konfirm" enctype="multipart/form-data" method="POST">
                                        <input type="text" placeholder="" name="id_token" id="id_token" value="{{$id_orderr}}"/>
                                        <input type="file" name="file" id="file" />
                                        <button type="submit" class="btn btn-default">Confirm</button>
                                </form>
                        </div><!--/login form-->
                </div>
               
                
            
            @else
      
            @endif
                    
            </div>
    </div>
</section><
@endsection