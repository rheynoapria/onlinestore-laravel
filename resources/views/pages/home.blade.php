@extends('layout.withsidebar')
@section('main')
<div class="col-sm-9 padding-right">
	{{-- <div class="features_items"><!--features_items-->
		<h2 class="title text-center">Features Items</h2>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
						<div class="productinfo text-center">
							<img src="/BahanStudy/images/home/laptop1.png" alt="" />
							<h2>$56</h2>
							<p>Lenovo ThinkPad X280</p>
							@if (session()->has('id'))
								<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							
							
						</div>
						<div class="product-overlay">
							<div class="overlay-content">
								<h2>$56</h2>
								<p>Lenovo ThinkPad X280</p>
								@if (session()->has('id'))
								<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
								@else
									
								@endif
								<a href="detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
							</div>
						</div>
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
					<div class="productinfo text-center">
						<img src="/BahanStudy/images/home/laptop2.png" alt="" />
						<h2>$56</h2>
						<p>Lenovo Legion Y700</p>
						@if (session()->has('id'))
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						@else
							
						@endif
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>$56</h2>
							<p>Lenovo Legion Y700</p>
							@if (session()->has('id'))
							<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							<a href="detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
						</div>
					</div>
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
					<div class="productinfo text-center">
						<img src="/BahanStudy/images/home/laptop3.png" alt="" />
						<h2>$56</h2>
						<p>Lenovo Yoga 710</p>
						@if (session()->has('id'))
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						@else
							
						@endif
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>$56</h2>
							<p>Lenovo Yoga 710</p>
							@if (session()->has('id'))
							<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							<a href="/detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
						</div>
					</div>
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
					<div class="productinfo text-center">
						<img src="/BahanStudy/images/home/laptop5.png" alt="" />
						<h2>$56</h2>
						<p>Acer Predator Triton 900</p>
						@if (session()->has('id'))
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						@else
							
						@endif
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>$56</h2>
							<p>Acer Predator Triton 900</p>
							@if (session()->has('id'))
							<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							<a href="detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
						</div>
					</div>
					<img src="/BahanStudy/images/home/new.png" class="new" alt="" />
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
					<div class="productinfo text-center">
						<img src="/BahanStudy/images/home/laptop6.png" alt="" />
						<h2>$56</h2>
						<p>Lenovo Yoga 920</p>
						@if (session()->has('id'))
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						@else
							
						@endif
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>$56</h2>
							<p>Lenovo Yoga 920</p>
							@if (session()->has('id'))
							<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							<a href="detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
						</div>
					</div>
					<img src="/BahanStudy/images/home/sale.png" class="new" alt="" />
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="product-image-wrapper">
				<div class="single-products">
					<div class="productinfo text-center">
						<img src="/BahanStudy/images/home/laptop8.png" alt="" />
						<h2>$56</h2>
						<p>Acer Aspire E-1431</p>
						@if (session()->has('id'))
						<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
						@else
							
						@endif
					</div>
					<div class="product-overlay">
						<div class="overlay-content">
							<h2>$56</h2>
							<p>Acer Aspire E-1431</p>
							@if (session()->has('id'))
							<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
							@else
								
							@endif
							<a href="detail-item" class="btn btn-default add-to-cart"><i class="fa fa-info"></i>Details</a>
						</div>
					</div>
				</div>
				<div class="choose">
					<ul class="nav nav-pills nav-justified">
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
						<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
					</ul>
				</div>
			</div>
		</div>
		
	</div><!--features_items--> --}}
	
	<div class="recommended_items"><!--recommended_items-->
		<h2 class="title text-center">recommended items</h2>
		
		<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active">	
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop9.2.png" alt="" />
									<h2>Rp 10.000.000</h2>
									<p><b>Asus ROG Strix Hero II</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop5.png" alt="" />
									<h2>Rp 14.000.000</h2>
									<p><b>Acer Predator Triton 900</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop4.0.png" alt="" />
									<h2>Rp 8.000.000</h2>
									<p><b>Lenovo Legion Y700</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
				</div>
				<div class="item">	
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop12.png" alt="" />
									<h2>Rp 12.000.000</h2>
									<p><b>Acer Swift 5</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop4.2.png" alt="" />
									<h2>Rp 9.000.000</h2>
									<p><b>Lenovo Y520</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="product-image-wrapper">
							<div class="single-products">
								<div class="productinfo text-center">
									<img src="/BahanStudy/images/home/laptop9.2.png" alt="" />
									<h2>Rp 12.000.000</h2>
									<p></b>Asus ROG Strix</b></p>
									{{-- @if (session()->has('id'))
									<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
									@else
										
									@endif --}}
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			  </a>
			  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			  </a>			
		</div>

		
	</div><!--/recommended_items-->
	
</div>
@endsection