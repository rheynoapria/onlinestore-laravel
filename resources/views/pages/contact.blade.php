@extends('layout.master')
@section('main')
<div id="contact-page" class="container" style="margin-top:190px;">
    <div class="bg"> 	
        <div class="row">  	
                <div class="col-sm-6">    			   			
                        <h2 class="title text-center">Contact <strong>Us</strong></h2>    			    				    				
                        <div class="contact-map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d63458.501654471205!2d106.94501459068218!3d-6.243115370769875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e698fb51e103b11%3A0x394217996e698361!2sKlinik%20avicenna!5e0!3m2!1sen!2sid!4v1574228078800!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                        </div>
                    </div>	
            <div class="col-sm-6">
                <div class="contact-info" style="margin-left:50px;">
                    <h2 class="title text-center">Contact Info</h2>
                    <address>
                        <p>Lenore Shop Inc.</p>
                        <p>Kp Jati Bulak RT 05 RW 01 Kelurahan Jati Mulya</p>
                        <p>Kecamatan Tambun Selatan Kabupaten Bekasi Jawa Barat</p>
                        <p>Mobile: +62822 9804 0528</p>
                        <p>Email: info@lenore.co.id</p>
                    </address>
                    <div class="social-networks">
                        <h2 class="title text-center">Social Networking</h2>
                        <ul>
                            <li>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>    			
        </div>  
    </div>	
</div><!--/#contact-page-->
@endsection