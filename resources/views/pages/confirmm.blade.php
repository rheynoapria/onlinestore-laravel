@extends('layout.master')
@section('main')
<section id="form"><!--form-->
@if (session()->has('id'))
    <div class="container" style="margin-top:190px;">
            <div class="row">
                     <div class="col-sm-4 col-sm-offset-1">
                             <div class="contact-info">
                                     <h2 class="title text-center">Tata Cara Pembayaran</h2>
                                     <address>
                                     <p>Pembayaran pesanan anda dapat dilakukan dengan mentransfer nominal harga total pada order id ke nomor rekning dibawah ini</p><br><br>
                                     <img src="{{ asset('images/frontend_images/bca_logo.png')}}" class="rounded" alt="confirm images" style="width:70px; height:30px;">
                                     <p><b> BANK BCA NO. REK 814214121 A.N Lenore shop</b></p><br><br>
                                     <p></p>
                                     <p></p>
                                     <p>Mohon konfirmasi pembayaran anda dengan cara mengupload bukti transaksi anda</p>
                                     <p>Email: info@lenore.co.id</p>
                                     </address>
                             </div>
                     </div>
                     <div class="col-sm-4 col-sm-offset-1">
                             <div class="login-form"><!--login form-->
                                     <h2>Konfirmasi Pembayaran</h2>
                                     <form action="/Konfirm" enctype="multipart/form-data" method="POST">
                                             <input type="text" placeholder="Masukan order id" name="id_token" id="id_token"/>
                                             <input type="file" name="file" id="file" />
                                             <button type="submit" class="btn btn-default">Confirm</button>
                                     </form>
                             </div><!--/login form-->
                     </div>

                     <div class="row">
                        <div class="col-sm-8 col-sm-offset-1">
                        @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ $message }}</strong>
                                </div>
                        @endif
                        </div>
                     </div>
                 
             

                
            </div>
           
    </div>
    @else
    <div class="container">
    <div class="text-center">
                <img src="{{ asset('images/frontend_images/cart.svg') }}" class="rounded" alt="confirm images" style="width:450px; height:450px;">
                <h2 style="font-size:42px; text-alignment:center;"> Oopps..</h2>
                <p style="font-size:18px;">Login terlebih dahulu untuk dapat membuka halaman konfirmasi</p>
                <button type="button" class="btn btn-fefault cart" style="width:200px;"><a href="/login" style="color:white; font-size:18px;">
                        <i class="fa fa-sign-in"></i>
                        Login Here
                </a></button>
    </div>
    </div>
    @endif
</section>
@endsection