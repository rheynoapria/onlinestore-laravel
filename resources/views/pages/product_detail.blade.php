@extends('layout.nobanner')

@section('main')
<div class="col-sm-9 padding-right" style="margin-top:190px;">
    <div class="product-details"><!--product-details-->
        <div class="col-sm-5">
            <div class="view-product">
                <img src="/data_file/{{$data_item->gambar}}" alt="" style="height:320px; width:350px;" />
            </div>

        </div>
        <div class="col-sm-7">
            <div class="product-information"><!--/product-information-->
                <img src="/BahanStudy/images/product-details/new.jpg" class="newarrival" alt="" />
                <h2>{{$data_item->nama_produk}}</h2>
                <p>{{$data_item->jenis}}</p>
                <span>
                        <?php
                        $angka = floatval($data_item->harga);
                        $rupiah =  number_format($angka,0,'.','.');
                    ?>
                    <span> Rp {{$rupiah}}</span>
                    @if (session()->has('id'))
                    <button data-toggle="modal" data-target="#myModal"  data-id="{{ $data_item->id_barang }}" class="btn btn-default add-to-cart jumlah"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                    @else
                    <button type="button" class="btn btn-fefault cart"><a href="/login" style="color:white;">
                        <i class="fa fa-shopping-cart"></i>
                        Add to cart
                    </a></button>
                    @endif
                    
                </span>
                <p><b>Availability : </b> In Stock</p>
                <p><b>Condition : </b> New</p>
                <p><b>Brand: </b>{{$data_item->brand}}</p>
                <p><b>Processors : </b> {{$data_item->cpu}}</p>
                    <p><b>Memory : </b> {{$data_item->memory}}</p>
                    <p><b>Storage : </b> {{$data_item->storage}}</p>
                    <p><b>VGA : </b> {{$data_item->vga}}</p>
                    <p><b>Optical Drive : </b> {{$data_item->optical_drive}}</p>
                    <p><b>Display : </b> {{$data_item->display}}</p>
                    <p><b>Operating System : </b> {{$data_item->os}}</p>
                
                {{-- <a href=""><img src="/BahanStudy/images/product-details/share.png" class="share img-responsive"  alt="" /></a> --}}
            </div><!--/product-information-->
        </div>
    </div><!--/product-details-->
{{-- 
    <div class="category-tab shop-details-tab"><!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li><a href="#details" data-toggle="tab">Spesifikasi</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade" id="details" >
                
                    
            </div>
            
            
        </div>
    </div><!--/category-tab--> --}}
</div>
@endsection