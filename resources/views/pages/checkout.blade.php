@extends('layout.master')
@section('main')
<section id="cart_items">
    <div class="container">
            <div class="breadcrumbs">
                    <ol class="breadcrumb">
                      <li><a href="#">Home</a></li>
                      <li class="active">Check out</li>
                    </ol>
            </div><!--/breadcrums-->


            <?php $total = 0; ?>
            <div class="review-payment" style="margin-top:190px;">
                    <h2>Review & Payment</h2>
            </div>

            <?php
              $order_id = $checkout_data->groupBy('id_checkout');
             ?>
            
            @foreach ($checkout_data as $item)
                
            @endforeach
            @if (session()->has('id'))
               
                @foreach($order_id as $oid)
                <?php $nama_user = $oid->mapWithKeys(function ($item) {
                        return [$item->nama_user];
                    });           
                    $nama_user = $nama_user->implode('nama_user');
                ?>
                @if (session()->get('nama')===$nama_user)
                <div class="table-responsive cart_info">
                           <table class="table table-condensed">
                                   <thead>
                                           <tr class="cart_menu">
                                           <?php $id_order = $oid->mapWithKeys(function ($item) {
                                                   return [$item->id_checkout];
                                               });           
                                               $id_order = $id_order->implode('id_checkout');
                                               ?>
                                           <td class="image">Order ID: <b>{{$id_order}}</b></td>
                                                   <td class="description">Nama Item</td>
                                                   <td class="price">Price</td>
                                                   <td class="quantity">Quantity</td>
                                                   <td class="total">Total</td>
                                                   <td></td>
                                           </tr>
                                   </thead>
                                   @foreach ($oid as $ckt)
                                   <tbody>
                                                   <tr>
                                                           <td class="cart_product">
                                                                   <a href=""><img src="/data_file/{{$ckt->gambar}}" alt="" style="width:220px;height:200px;"></a>
                                                           </td>
                                                           <td class="cart_description">
                                                                   <h4><a href="">{{$ckt->nama_produk}}</a></h4>
                                                           </td> 
                                                           <td class="cart_price">
                                                                   <?php $harga_rupiah = floatval($ckt->harga); 
                                                                   $rupiah =  number_format($harga_rupiah,0,'.','.'); ?>
                                                                   <p>Rp {{$rupiah}}</p>
                                                           </td>
                                                           <td class="cart_quantity">
                                                                   {{$ckt->jumlah}}
                                                           </td>
                                                           <td class="cart_total">
                                                                   <?php $harga_total = floatval($ckt->harga * $ckt->jumlah); 
                                                                   $rupiah_total =  number_format($harga_total,0,'.','.'); ?>
                                                           <p class="cart_total_price">Rp {{$rupiah_total}}</p>
                                                           </td>
                                                   </tr>          
                                                   <?php $total += ($ckt->jumlah * $ckt->harga) ?>
                                @endforeach 
                                               
                                        <?php $status_order = $oid->mapWithKeys(function ($item) {
                                                return [$item->status];
                                                });           
                                                $status_order = $status_order->implode('status');
                                        ?>
                                                          @if ($status_order === '1')
                                                          <tr>
                                                                        <td>
                                                                                <p style="margin-left:15px;"">Status:</p>
                                                                                <h3 style="margin-left:15px; color:blue;">Pesanan sedang di proses</h3>
                                                                        </td>
                                                                        <td colspan="3">&nbsp;</td>
                                                                        <td colspan="2">
                                                                                <table class="table table-condensed total-result">
                                                                                        <tr>
                                                                                                <td>Total</td>
                                                                                                <?php $harga_total = floatval($total); 
                                                                                                $total_rupiah =  number_format($harga_total,0,'.','.'); ?>
                                                                                                <td><span>Rp {{$total_rupiah}}</span></td>
                                                                                        </tr>
                                                                                </table>
                                                                        </td>
                                                          </tr> 
                                                          @else
                                                          <tr>
                                                                        <td>
                                                                                <p style="margin-left:15px;"">Status:</p>
                                                                                <h3 style="margin-left:15px; color:red;">Menunggu Pembayaran</h3>
                                                                        </td>
                                                                        <td colspan="3">&nbsp;</td>
                                                                        <td colspan="2">
                                                                                <table class="table table-condensed total-result">
                                                                                        <tr>
                                                                                                <td>Total</td>
                                                                                                <?php $harga_total = floatval($total); 
                                                                                                $total_rupiah =  number_format($harga_total,0,'.','.'); ?>
                                                                                                <td><span>Rp {{$total_rupiah}}</span></td>
                                                                                        </tr>
                                                                                </table>
                                                                        </td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="4">&nbsp;</td>
                                                                <td colspan="2">
                                                                        <form action="/Konfirmasi_pembayaran" method="post">
                                                                                <button type="submit" class="btn btn-default" name="id_order" value="{{$id_order}}">Konfirmasi pembayaran</button>
                                                                        </form>
                                                                </td>
                                                        </tr>
                                                          @endif
                                                         
                                           </tbody>
                                   
                           </table>
                        <?php $total = 0 ?> 
                   </div>
                   @else 
                   @endif
                   @endforeach
            @else
            <div class="container">
                        <div class="text-center">
                                    <img src="{{ asset('images/frontend_images/confirm.svg') }}" class="rounded" alt="confirm images" style="width:450px; height:450px;">
                                    <h2 style="font-size:42px; text-alignment:center;"> Oopps..</h2>
                                    <p style="font-size:18px;">Login terlebih dahulu untuk dapat membuka halaman checkout</p>
                                    <button type="button" class="btn btn-fefault cart" style="width:200px;"><a href="/login" style="color:white; font-size:18px;">
                                            <i class="fa fa-sign-in"></i>
                                            Login Here
                                    </a></button>
                        </div>
            </div>
            @endif


        <div class="payment-options">
        </div>
    </div>
</section> <!--/#cart_items-->
@endsection