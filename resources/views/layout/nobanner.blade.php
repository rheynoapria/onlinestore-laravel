<?php
use App\ProductCount;

$cartCount = ProductCount::cartCount();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | Lenore</title>
    <link href="/BahanStudy/css/bootstrap.min.css" rel="stylesheet">
	<link href="/BahanStudy/css/font-awesome.min.css" rel="stylesheet">
	<link href="/BahanStudy/css/prettyPhoto.css" rel="stylesheet">
	<link href="/BahanStudy/css/price-range.css" rel="stylesheet">
    <link href="/BahanStudy/css/animate.css" rel="stylesheet">
	<link href="/BahanStudy/css/main.css" rel="stylesheet">
	<link href="/BahanStudy/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/BahanStudy/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/BahanStudy/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/BahanStudy/images/ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="/BahanStudy/images/ico/apple-touch-icon-57-precomposed.png">
	
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		
		<nav>
		<div class="header-middle"><!--header-middle-->
			<div class="container ">
				<div class="row">
					<div class="col-md-4 clearfix">
						<div class="logo pull-left">
							<a href="/"><img src="/BahanStudy/images/home/logo.png" alt="" /></a>
						</div>
						
					</div>
					<div class="col-md-8 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								<li><a href="/checkout_list"><i class="fa fa-crosshairs"></i> Checkout</a></li>
								@if ($cartCount===0)
								<li><a href="/cart"><i class="fa fa-shopping-cart"></i> Cart</a></li>
								@else
								<li><a href="/cart"><i class="fa fa-shopping-cart"></i> Cart ({{$cartCount}})</a></li>
								@endif	
								@if (session()->has('id'))
									<li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
									<li><a href=""><i class="fa fa-user"></i>{{session()->get('nama')}}</a></li>									
								@else
									<li><a href="/login"><i class="fa fa-lock"></i> Login</a></li>
								@endif
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
		
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="/" class="active">Home</a></li>
								<li><a href="/shop">Product</a></li>
								<li><a href="/confirm">Confirm</a></li>
								<li><a href="/contact">Contact</a></li>
							</ul>
						</div>
					</div>
					{{-- <div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div> --}}
				</div>
			</div>
		</div><!--/header-bottom-->
		</nav>
	</header><!--/header-->
	
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
                    <div class="left-sidebar sticky-sidebar">
                        <h2>Category</h2>
                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/shop/notebook">Notebook</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/shop/ultrabook">Ultrabook</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/shop/workstation">Workstation</a></h4>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><a href="/shop/gaming_laptop">Gaming Laptop</a></h4>
                                </div>
                            </div>
                        </div><!--/category-products-->
                    
                        <div class="brands_products"><!--brands_products-->
                            <h2>Brands</h2>
                            <div class="brands-name">
                                <ul class="nav nav-pills nav-stacked">
									<li><a href="/shop/acer"> <span class="pull-right">({{$brands->get('acer')}})</span>Acer</a></li>
                                    <li><a href="/shop/lenovo"> <span class="pull-right">({{$brands->get('lenovo')}})</span>Lenovo</a></li>
                                    <li><a href="/shop/dell"> <span class="pull-right">({{$brands->get('dell')}})</span>Dell</a></li>
                                    <li><a href="/shop/asus"> <span class="pull-right">({{$brands->get('asus')}})</span>Asus</a></li>
                                    <li><a href="/shop/hp"> <span class="pull-right">({{$brands->get('hp')}})</span>HP</a></li>
                                    
                                </ul>
                            </div>
                        </div><!--/brands_products-->
                        
					</div>
                </div>
				
				<div class="main">
				@yield('main')
				</div>
			</div>
		</div>
	</section>
	
	<footer id="footer"><!--Footer-->
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2019 Lenore-Shop. All rights reserved.</p>
					<p class="pull-right">Designed by <span><a target="_blank" href="http://www.themeum.com">Themeum</a></span></p>
				</div>
			</div>
		</div>
		
	</footer><!--/Footer-->
	<!-- Modal -->
         <div class="modal fade" id="myModal" role="dialog">
           <div class="modal-dialog">
             <form action="/AddCart" method="post">
             <!-- Modal content-->
             <div class="modal-content">
               <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title">Masukan Jumlah</h4>
               </div>
               <div class="modal-body">
                 <input type="hidden" id="id_barang" class="id_barang" name="id_barang" value="3487">
                 <div class="form-group">
                   <label for="exampleInputEmail1">Jumlah Beli</label>
                   <input type="text" class="form-control " id="jumlah" name="jumlah" placeholder="Jumlah Beli">
                 </div>
               </div>
               <div class="modal-footer">
                 <button type="submit" class="btn btn-default">Beli</button>
                 <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
           </form>
           </div>
         </div>

        </div>
	

  
    <script src="/BahanStudy/js/jquery.js"></script>
	<script src="/BahanStudy/js/bootstrap.min.js"></script>
	<script src="/BahanStudy/js/jquery.scrollUp.min.js"></script>
	<script src="/BahanStudy/js/price-range.js"></script>
    <script src="/BahanStudy/js/jquery.prettyPhoto.js"></script>
	<script src="/BahanStudy/js/main.js"></script>
	<script type="text/javascript">
		$(".jumlah").click(function() {
		$(".id_barang").val($(this).attr('data-id'));
		});

	</script>
</body>
</html>