<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductCount extends Model
{
    
    public static function cartCount(){
        
        $id = Session::get('id');
        $cart_count = DB::table('tbl_keranjang')->where('id_user', $id)->sum('jumlah');

        return $cart_count;
    }
    
}
