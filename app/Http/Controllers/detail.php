<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class detail extends Controller
{
    public function index(Request $request){
        
        $data_barang = DB::table('tbl_barang')->where('id_barang', '=', $request->id_barang)->first();
        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();

        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        return view('pages.product_detail', 
            ['data_item' => $data_barang],
            ['brands' => $brands]);
    }
}
