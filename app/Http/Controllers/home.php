<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\m_barang;
use Illuminate\Support\Facades\DB;

class home extends Controller
{
    public function index(){
        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();

        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        return view('pages.home',['brands' => $brands]);
    }

    public function store(Request $request){
        $this->validate($request,[
            'file' => 'required|max:2048'
        ]);
        $file = $request->file('file');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';

        if ($file->move($tujuan_upload,$nama_file)) {
            $data = m_barang::create([
                'nama_produk' => $request->nama_produk,
                'brand' => $request->brand,
                'jenis' => $request->jenis,
                'harga' => $request->harga,
                'jml_produk' => $request->jml_produk,
                'cpu' => $request->cpu,
                'memory' => $request->memory,
                'storage' => $request->storage,
                'vga' => $request->vga,
                'optical_drive' => $request->dvd,
                'display' => $request->display,
                'os' => $request->os,
                'gambar' => $nama_file
            ]);
            $res['message'] = "Success!";
            $res['values'] = $data;
            return response($res);
        }
    }

    public function contact(){
        return view ("pages.contact");
    }
}
