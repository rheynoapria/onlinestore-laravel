<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class order extends Controller
{
    public function Order(Request $request){
        DB::table('tbl_keranjang')->insert([
            'id_user' => Session::get('id'),
            'id_barang'=> $request->id_barang,
            'jumlah'=>$request->jumlah
        ]);

        return back();
    }

    public function keranjang(){
        $Keranjang = DB::table('keranjang')->get();
        return view('pages.cart', ['keranjang' => $Keranjang]);
    }

    public function remove_item(Request $request){
        DB::table('tbl_keranjang')->where('id_keranjang', '=', $request->id_keranjang)->delete();
        return redirect('/cart');
    }

    public function Checkout(Request $request){
        $id_check = rand();
        $total = 0;
        $data = DB::table('tbl_keranjang')->where('id_user',Session::get('id'))->get();

        foreach ($data as $krj) {
            $barang = DB::table('tbl_barang')->where('id_barang',$krj->id_barang)->get();
            foreach ($barang as $brg) {
                 $total = ($krj->jumlah * $brg->harga);
                 DB::table('detail_checkout')->insert([
                        'id_checkout' => $id_check,
                        'id_barang'=> $krj->id_barang,
                        'jumlah' => $krj->jumlah

                 ]);
            }
        }
        DB::table('tbl_checkout')->insert([

            'id_checkout'=> $id_check,
            'id_user' => Session::get('id'),
            'total' => $total

        ]);

        DB::table('tbl_keranjang')->where('id_user',Session::get('id'))->delete();

        return redirect('/checkout_list');
    }

    public function checkout_list(){
        $checkout  = DB::table('checkout')->get();
        return view('pages.checkout',['checkout_data'=> $checkout ]);
    }

    public function confirm(Request $request){

        $id_order = $request->input('id_order');
        return view('pages.confirm', ['id_orderr'=> $id_order]);
    }

    public function confirmm(){
        return view('pages.confirmm');
    }

    public function confirm_simpan(Request $request){
        $this->validate($request,[
            'file' => 'required|max:2048'
        ]);
        
        $orderID = $request->id_token;

        $file = $request->file('file');
        $nama_file = $orderID."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';

        if ($file->move($tujuan_upload,$nama_file)) {
            DB::table('tbl_konfirmasi')->insert([
                'id_user' => Session::get('id'),
                'id_checkout' => $request->id_token,
                'bukti'=> $nama_file
            ]);

            DB::table('detail_checkout')->where('id_checkout',$request->id_token)->update([
                'status' => 1
            ]);

            return redirect('/confirm')->with(['success' => 'Pembayaran Berhasil']);
        }
    }
}
