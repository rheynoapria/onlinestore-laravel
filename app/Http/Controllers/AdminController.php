<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function login(){
        return view('admin.admin_login');
    }

    public function validasi(Request $request){

            $data = $request->input();
            if ($data['username']== 'admin' and $data['password']=='l3nore') {
                session()->put('role','admin');
                return redirect('/admin/dashboard');
            } else{
                echo 'Gagal login';
            }
    }

    public function dashboard(){
        return view('admin.dashboard');
    }

    public function logout(){
        session()->forget('role');
        return redirect('/admin');
    }
}
