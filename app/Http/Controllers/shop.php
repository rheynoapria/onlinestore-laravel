<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 

class shop extends Controller
{
    public function index(){

        $barang = DB::table('tbl_barang')->get();
        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
       
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);

    }

    public function acer(){
        $barang = DB::table('tbl_barang')->where('brand', '=', 'Acer')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function lenovo(){
        $barang = DB::table('tbl_barang')->where('brand', '=', 'Lenovo')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function dell(){
        $barang = DB::table('tbl_barang')->where('brand', '=', 'Dell')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function asus(){
        $barang = DB::table('tbl_barang')->where('brand', '=', 'Asus')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function hp(){
        $barang = DB::table('tbl_barang')->where('brand', '=', 'HP')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function notebook(){
        $barang = DB::table('tbl_barang')->where('jenis', '=', 'Notebook')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function ultrabook(){
        $barang = DB::table('tbl_barang')->where('jenis', '=', 'Ultrabook')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function workstation(){
        $barang = DB::table('tbl_barang')->where('jenis', '=', 'Workstation')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }

    public function gaming(){
        $barang = DB::table('tbl_barang')->where('jenis', '=', 'Gaming Laptop')->get();

        $acer = DB::table('tbl_barang')->where('brand','Acer')->count();
        $lenovo = DB::table('tbl_barang')->where('brand','Lenovo')->count();
        $dell = DB::table('tbl_barang')->where('brand','Dell')->count();
        $asus = DB::table('tbl_barang')->where('brand','Asus')->count();
        $hp = DB::table('tbl_barang')->where('brand','HP')->count();


        
        $brands = collect(['acer' => $acer, 'lenovo' => $lenovo,'dell' => $dell,'asus' => $asus,'hp' => $hp]);
        // return $barang;
        return view('pages.shop',['barang' => $barang],['brands' => $brands]);
    }
}
